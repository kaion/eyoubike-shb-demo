import mraa

def set_gpio_out(gpio_num, gpio_lv):
  pin_37 = mraa.Gpio(gpio_num)
  pin_37.dir(mraa.DIR_OUT)
  pin_37.write(gpio_lv)

if __name__ == "__main__":
  import sys
  print sys.argv
  set_gpio_out(int(sys.argv[1]), int(sys.argv[2]))
  
