#!/usr/bin/python
# -*- coding: utf-8 -*-
import os , sys , random
import serial
import time
import gpio_fixed
from PIL import Image , ImageFont , ImageDraw

VERSION = "2018/09/04 10:20"
# 0616: MSB -> LSB
# rc.local -> sh -> python thread can't use print

display = {'speed': 0 , 'battery': 0 , 'ride_distance': 0 , 'ride_time': 0}
EUB_UART1 = '/dev/ttyUSB0'
EUB_VCOM = '/dev/ttyACM0'
EUB_BT = '/dev/ttyS1' ;

SHOW_VERSION = True
vershow = SHOW_VERSION

WS = 2 

def chksum ( data , first=0x23 ):
    chksum = first
    for i in range ( 0 , len ( data ) ):
        chksum = chksum ^ ord ( data[ i ] )
    return chr ( chksum )


def request_bruce ( console , data , recvlen , debug=False ):
    connet_cmd = '\x23' + data + chksum ( data ) + '\x2a'
    console.write ( connet_cmd )
    if debug:
        True ;
        print 'Send(%3d): ' % len ( connet_cmd ) + ' '.join ( '{:02X}'.format ( ord ( c ) ) for c in (connet_cmd) )
    response_data = console.read ( recvlen )
    if debug:
        True
        print 'Recv(%3d): ' % len ( response_data ) + ' '.join ( '{:02X}'.format ( ord ( c ) ) for c in response_data )
    return response_data

ride_t = 0 ;
ride_dis = 0.0 ;
charging_ct = 0 ;
g_usb0_console = None ;
def eyoubike_can ():

    global g_usb0_console ;
    g_usb0_console = serial.Serial ( port=EUB_UART1 , baudrate='115200' , bytesize=8 , parity='N' , stopbits=1 , timeout=0.005 , xonxoff=0 , rtscts=0 )

    global display
    while True:
        resp = request_bruce ( g_usb0_console , '\x03\x32\x07\x81' , 30 ,True)
        try:
            if resp[ -2:-1 ] == chksum ( resp[ 1:-2 ] , ord ( resp[ 0 ] ) ):
                if len ( resp ) >= 17:
                    display[ 'battery' ] = ord ( resp[ 0 + 8 ] )
            else:
                True ;
                print 'Failed(%3d): ' % len ( resp ) + ' '.join ( '{:02X}'.format ( ord ( c ) ) for c in resp )
        except:
            True
            print "-E- 0x781"
        resp = request_bruce ( g_usb0_console , '\x03\x32\x06\xC2' , 30 ,True)
        try:
            if resp[ -2:-1 ] == chksum ( resp[ 1:-2 ] , ord ( resp[ 0 ] ) ):
                if len ( resp ) >= 17:
                    display[ 'speed' ] = ((ord ( resp[ 1 + 8 ] ) * 0x100 + ord ( resp[ 0 + 8 ] )) / 10)
            else:
                True
                print 'Failed(%3d): ' % len ( resp ) + ' '.join ( '{:02X}'.format ( ord ( c ) ) for c in resp )
        except:
            True
            print "-E- 0x6c2"
        resp = request_bruce ( g_usb0_console , '\x03\x32\x06\xC3' , 30 ) ;
        try:
            if resp[ -2:-1 ] == chksum ( resp[ 1:-2 ] , ord ( resp[ 0 ] ) ):
                if len ( resp ) >= 17:
                    display[ 'ride_distance' ] = ( float ( (ord ( resp[ 3 + 8 ] ) * 0x100 + ord ( resp[ 2 + 8 ] ) ) ) / 10)
                    display[ 'ride_time' ] = (ord ( resp[ 5 + 8 ] ) * 0x100 + ord ( resp[ 4 + 8 ] ))
            else:
                True
                print 'Failed(%3d): ' % len ( resp ) + ' '.join ( '{:02X}'.format ( ord ( c ) ) for c in resp )
        except:
            True
            print "-E- 0x6c3"

	    #充電器是否存在來作判斷車子資料是否清空#
        global charging_ct ;
        global ride_t ;
        global ride_dis ;

        if charging_ct <= 5 :
            charging_ct += 1 ;
            if 5 == charging_ct :
                ride_t = display [ 'ride_time' ] ;
                ride_dis = display [ 'ride_distance' ] ;
    	resp = request_bruce ( g_usb0_console , '\x03\x32\x07\xC1' , 30 ) ;
        try :
            if 11 == len ( resp ) :
                if resp[ -2:-1 ] == chksum ( resp[ 1:-2 ] , ord ( resp[ 0 ] ) ) :
                    if ord ( resp [ - 3 ] ) != 0 :
                        charging_ct = 0 ;
                        ride_t = display [ 'ride_time' ] ;
                        ride_dis = display [ 'ride_distance' ] ;
            else :
                True ;
                print 'Failed(%3d): ' % len ( resp ) + ' '.join ( '{:02X}'.format ( ord ( c ) ) for c in resp ) ;
        except :
            True ;
            print "-E- 0x6c4" ;

        time.sleep ( 0.2 )


def lcm ():
    global vershow
    gpio_fixed.set_gpio_out(44, 0)
    time.sleep(0.1)
    gpio_fixed.set_gpio_out(44, 1)
    time.sleep(WS)
    while os.path.exists(EUB_VCOM) is False:
      print 'waiting %s...' % EUB_VCOM
      time.sleep(0.2)
    acm0_console = serial.Serial ( port=EUB_VCOM , timeout=0.05 )
    minifont = ImageFont.truetype ( "XinGothic-HTC-Regular.ttf" , 70 )
    wb = 1
    columns = 640
    rows = 480
    pixel_bit = 1
    rlength = columns * pixel_bit / 8
    section = rows / 8
    request_bruce ( acm0_console , '\x05\x30\x00\x01\x00\x00' , 6 )


    BT_console = serial.Serial ( port=EUB_BT , baudrate='115200' , bytesize=8 , parity='N' , stopbits=1 , timeout=0.005 , xonxoff=0 , rtscts=0 ) ;
    global display ;
    while True:
        bw = (0 if wb == 1  else 1)
        im = Image.new ( "1" , (columns , rows) , wb )
        draw = ImageDraw.Draw ( im )
        if vershow == True:
            draw.text ( (0 , 0) , u"version" , font=minifont , fill=bw )
            draw.text ( (0 , 100) , u"%s" % VERSION , font=minifont , fill=bw )
        else:
            # 騎乘速度 0xA0 #
#            speed_n = round ( display [ 'speed' ] , 1 ) ;
            request_BT ( BT_console , "A0" , display [ 'speed' ] ) ;
            draw.text ( (0 , 0) , u"速度: %.1fKM/h" % display[ 'speed' ] , font=minifont , fill=bw ) ;

            # 電壓 0xA3 #
            request_BT ( BT_console , "A3" , display[ 'battery' ] ) ;
            draw.text ( (0 , 100) , u"電池電量: %d%%" % display[ 'battery' ] , font=minifont , fill=bw ) ;

            display [ 'ride_distance' ] -= ride_dis ;
            print ( "ride_dis::" + str ( ride_dis ) ) ;
            # 騎乘距離 0xA2 #
            distance_n = round ( display [ 'ride_distance' ] , 1 ) ;

            request_BT ( BT_console , "A2" , distance_n ) ;
            draw.text ( (0 , 200) , u'騎乘距離: %.1fKM' % display[ 'ride_distance' ] , font=minifont , fill=bw ) ;

            display [ 'ride_time' ] -= ride_t ;
            print ( "ride_t::" + str ( ride_t ) ) ;
            # 騎乘時間 0xA1 #
            request_BT ( BT_console , "A1" , display[ 'ride_time' ] ) ;
            draw.text ( (0 , 300) , u"騎乘時間: %dmins" % display[ 'ride_time' ] , font=minifont , fill=bw ) ;

            resp = respones_BT ( BT_console ) ;
            try:
                if len ( resp ) >= 6:
                    buzz_hex = "%s%s%s" % ( "\x05\x28" , resp [ 3 ] , "\x01\x0A\x00" ) ;
                    global g_usb0_console ;
                    request_bruce ( g_usb0_console , buzz_hex , 10 ) ;
            except:
                True
                print "-E- 0x6c3" ;

        try:
            content = im.tostring ( )
        except:
            content = im.tobytes ( )
        z = 8
        for y in range ( 0 , rows , z ):
            while True:
                data = '\x21' + chr ( (y >> 8) & 0xff ) + chr ( y & 0xff ) + content[ y * rlength: (y + z) * rlength ]
                data = chr ( (len ( data ) >> 8) & 0xff ) + chr ( len ( data ) & 0xff ) + data
                response = request_bruce ( acm0_console , data , 6 )
                if ord ( response[ 2 ] ) == 0x21 and ord ( response[ 3 ] ) == 0x01:  # and i >= 10:
                    break
                else:
                    True ;
                    print 'Failed(%3d): ' % len ( response ) + ' '.join ( '{:02X}'.format ( ord ( c ) ) for c in response )
        if vershow == True:
            time.sleep ( 1 )
            vershow = False


def hexstrToHex ( data_s ):
    data = "" ;
    sz = len ( data_s ) ;
    if sz & 0x01:
        return data ;

    for i in range ( 0 , len ( data_s ) , 2 ):
        data += chr ( int ( data_s[ i: i + 2 ] , 16 ) ) ;

    return data ;


def EDC ( data_s ):
    sz = len ( data_s ) ;
    if sz & 0x01: return "00" ;

    EDC = 0 ;
    for i in range ( 0 , sz , 2 ): EDC ^= int ( data_s[ i: i + 2 ] , 16 ) ;

    return "%02X" % EDC ;



def respones_BT ( console , debug=False ):
    response_data = console.read ( 10 ) ;
    if debug:
        True
        print 'Recv(%3d): ' % len ( response_data ) + ' '.join ( '{:02X}'.format ( ord ( c ) ) for c in response_data ) ;

    return response_data ;

"""
    console :
    cmmd_s : 以字串形式 hexSTR 2Bytes 一組大寫
    value_i : 要傳入的數字
    debug = False : debug
"""
def request_BT ( console , cmmd_s , value_i , debug = False ):
    stx_s = "25" ;
    etx_s = "2A" ;
    data_s = "00" ;
    if cmmd_s == "A0":  # 騎乘速度 0xA0
        data_s = "%04X" % ( value_i * 100 ) ;
    elif cmmd_s == "A1":  # 騎乘時間 0xA1
        data_s = "%08X" % ( value_i * 60 ) ;
    elif cmmd_s == "A2":  # 騎乘距離 0xA2
        data_s = "%08X" % ( value_i * 100 ) ;
    elif cmmd_s == "A3":  # 電壓 0xA3
        data_s = "%04X" % ( value_i * 100 ) ;

    stx_s = stx_s + "%02X" % ( ( len ( cmmd_s ) + len ( data_s ) ) / 2) + cmmd_s + data_s ;
    stx_s = stx_s + EDC ( stx_s ) + etx_s ;

    stx_hex = hexstrToHex ( stx_s ) ;

    console.write ( stx_hex )
    if debug:
        True ;
        print 'Send(%3d): ' % len ( stx_hex ) + ' '.join ( '{:02X}'.format ( ord ( c ) ) for c in (stx_hex) ) ;

    return ;


def BT ():
    BT_console = serial.Serial ( port=EUB_BT , baudrate='115200' , bytesize=8 , parity='N' , stopbits=1 , timeout=0.005 , xonxoff=0 , rtscts=0 ) ;

    global display ;
    while True:
        # 騎乘速度 0xA0 #
        request_BT ( BT_console , "A0" , display[ 'speed' ] ) ;
        time.sleep ( 0.25 ) ;

        # 騎乘時間 0xA1 #
        request_BT ( BT_console , "A1" , display[ 'ride_time' ] ) ;
        time.sleep ( 0.25 ) ;

        # 騎乘距離 0xA2 #
        request_BT ( BT_console , "A2" , display[ 'ride_distance' ] ) ;
        time.sleep ( 0.25 ) ;

        # 電壓 0xA3 #
        request_BT ( BT_console , "A3" , display[ 'battery' ] ) ;
        resp = respones_BT ( BT_console ) ;
        try:
            if len ( resp ) >= 6:
                buzz_hex = "%s%s%s" % ( "\x05\x28" , resp [ 3 ] , "\x01\x0A\x00" ) ;
                global g_usb0_console ;
                request_bruce ( g_usb0_console , buzz_hex , 10 ) ;
        except:
            True
            print "-E- 0x6c3" ;


if __name__ == "__main__":
    from threading import Thread
    thread1 = thread2 = thread3 = None
    while True :
        if thread1 is None or False == thread1.isAlive ( ) :
            print ( "thread1 break" ) ;
            thread1 = Thread ( target = lcm ) ;
            thread1.daemon = True ;
            thread1.start ( ) ;
            time.sleep(WS+1)

        if thread2 is None or False == thread2.isAlive ( ) :
            print ( "thread2 break" ) ;
            thread2 = Thread ( target = eyoubike_can ) ;
            thread2.daemon = True ;
            thread2.start ( ) ;
            time.sleep(0.5)

#       if thread3 is None or False == thread3.isAlive ( ) :
#            print ( "thread3 break" ) ;
#            thread3 = Thread ( target = BT ) ;
#            thread3.daemon = True ;
#            thread3.start ( ) ;
#            time.sleep(0.5)

        time.sleep ( 1 ) ;
